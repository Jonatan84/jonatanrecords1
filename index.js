var express=require("express");
var app=express();
var db=require('./db.js');
//var gameapps=require("./gameapps.js");
//var records=require('records.js');
var bodyParser=require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extend: true}));

app.get('/hola', function(req,res) {
	res.send("Hola mon!");
});

//Busueda totes les aplicacions
app.get('/app/all', function(req,res) {
//	console.log(" ", records);
//	records.get
	db.query("select * from app", function(err,result){
	console.log(result);
		if(!err)
		{
			res.json(result.rows);
		}
		else{
			res.end("Error en la DB");
			console.log(err);
		}

	});
});
//Busueda tots els records de una app
app.get('/record/app/:idapp', function(req,res) {
//	console.log(" ", records);
//	records.get
	db.query("select * from record where app_code=$1",[req.params.idapp], function(err,result){
	console.log(result);
		if(!err)
		{
			res.json(result.rows);
		}
		else{
			res.end("Error en la DB");
			console.log(err);
		}

	});
});
//Inserta un nuevo Record
app.post('/record/save/:app_code?:player?:score', function(req,res) {

	db.query("INSERT INTO record(app_code,player,score) VALUES($1,$2,$3)",[req.params.app_code,req.params.player,req.params.score], function(err,result){
	console.log(result);
		if(!err)
		{
			res.json(result.rows);
		}
		else{
			res.end("Error en la DB");
			console.log(err);
		}

	});
});


app.listen(process.env.PORT|| 3000, function(){
	console.log("App listening on port 3000");
});
